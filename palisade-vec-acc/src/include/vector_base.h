#ifndef SRC_BASE_H
#define SRC_BASE_H

#include "vector_interface.h"

namespace vec_acc {

#define print_base_class(funcname)                                  \
  do {                                                         \
    std::cout << "VectorBase::" << funcname << std::endl; \
  } while (0)

template <typename IntType>
class VectorBase : public VectorInterface<VectorBase<IntType>, IntType> {
 public:
  ~VectorBase() {}
  void SetClassName(std::string name) {
    this->m_className = "VectorBase::" + name;
  }

  std::ostream& Print(std::ostream& os) {
    os << "VectorBase : [" << std::endl << "\t";
    const auto len = this->m_data.size();
    for (size_t i = 0; len > i; ++i) {
      os << this->m_data[i];
      if (i == len-1) break;
      os << ", ";
    }
    os << std::endl << "],";
    
    return os;
  }
};
}  // namespace vec_acc

#endif  // SRC_BASE_H
