#ifndef SRC_EXTENSION_H
#define SRC_EXTENSION_H

#include "vector_base.h"

namespace vec_acc {

#define print_extension_class(funcname)                                  \
  do {                                                         \
    std::cout << "VectorExtension::" << funcname << std::endl; \
  } while (0)

template <typename IntType>
class VectorExtension : public VectorBase<IntType> {
  static const std::string m_className;

 public:
  void PureVirtual();
  void Override();
};

}  // namespace vec_acc

#endif  // SRC_EXTENSION_H
