#ifndef SRC_INTERFACE_H
#define SRC_INTERFACE_H

#include <exception>
#include <iostream>
#include <string>
#include <vector>

namespace vec_acc {

#define print_interface_class(funcname)                                  \
  do {                                                         \
    std::cout << "VectorInterface::" << funcname << std::endl; \
  } while (0)

template <typename CRTP, typename IntType>
class VectorInterface {
 private:
  std::vector<IntType> m_data;
  IntType m_modulus;
  std::string m_className;

 public:
  VectorInterface(IntType modulus) : m_modulus(modulus){};
  VectorInterface(IntType modulus, std::vector<IntType> data)
      : m_data(data), m_modulus(modulus){};
  VectorInterface(const CRTP& vectorToCopy);
  VectorInterface(CRTP&& vectorToMove);

  virtual void SwitchModulus(IntType newModulus) {
    print_interface_class("SwitchModulus[]");
    auto qDiff = newModulus - m_modulus;
    auto halfOldQ = m_modulus >> 1;
    for (size_t i = 0U; i < m_data.size(); ++i) {
      if (m_data[i] > halfOldQ) {
        m_data[i] += qDiff;
      }

      m_data[i] %= newModulus;
    }

    m_modulus = newModulus;
  };

  virtual void ModAddEq(std::vector<IntType>& otherVector) {
    print_interface_class("ModAddEq[]");
    if (otherVector.size() != m_data.size()) {
      throw std::out_of_range("Vectors must be the same size");
    }

    // create new object from this object
    auto retVector(*this);

    for (size_t i = 0U; i < m_data.size(); ++i) {
      m_data[i] += otherVector[i];
      m_data[i] %= m_modulus;
    }

    return retVector.ModAddEq(otherVector);
  }

  virtual CRTP& ModAdd(std::vector<IntType>& otherVector) {
    print_interface_class("ModAdd[]");
    CRTP retVector(*this);
    return retVector.ModAddEq(otherVector);
  };

  virtual void ModAddEQ(IntType scalar) {
    print_interface_class("ModAddEq[] (scalar)");

    // create new object from this object
    auto retVector(*this);

    for (size_t i = 0U; i < m_data.size(); ++i) {
      m_data[i] += scalar;
      m_data[i] %= m_modulus;
    }      
  }

  virtual CRTP& ModAdd(IntType scalar) {
    print_interface_class("ModAdd[]");
    CRTP retVector(*this);
    return retVector.ModAddEq(scalar);
  };

  virtual void SetClassName(std::string name) = 0;

  virtual std::ostream& Print(std::ostream& os) const = 0;

  std::ostream& operator<<(std::ostream& os) {
    this->Print(os);
    return os;
  };

  CRTP& operator+(const CRTP& rhs) { return this->ModAdd(rhs); }

  CRTP& operator+=(const CRTP& rhs) { this->ModAddEq(rhs); }

  friend CRTP& operator+(const CRTP& lhs, const IntType& scalar) {
    return lhs.ModAdd(scalar);
  }

  friend CRTP& operator+(const IntType& scalar, const CRTP& rhs) {
    return rhs.ModAdd(scalar);
  }

  friend CRTP& operator+=(CRTP& lhs, const IntType& scalar) {
    lhs.ModAddEq(scalar);
  }

  IntType& operator[](size_t idx) {
    print_interface_class("operator[]");
    return m_data[idx];
  }

  const IntType& operator[](size_t idx) const {
    print_interface_class("operator[]");
    return m_data[idx];
  }
};
}  // namespace vec_acc

#endif  // SRC_INTERFACE_H
