#include "vector_extension.h"

using namespace vec_acc;


const std::string Extension::m_className = "Extension";

void Extension::PureVirtual() {
    std::cout << "Extension::PureVirtual" << std::endl;
    std::cout << "m_className: " << m_className << std::endl;
}

void Extension::Override() {
    std::cout << "Extension::Override" << std::endl;
    std::cout << "m_className: " << m_className << std::endl;
}

// void Extension::Abstract() {
//     std::cout << "Extension" << std::endl;
//     std::cout << "m_className: " << m_className << std::endl;
//     std::cout << "m_className: " << m_className << std::endl;
// }
