#include "vector_interface.h"

using namespace vec_acc;

#define print_class(funcname)                                  \
  do {                                                         \
    std::cout << "VectorInterface::" << funcname << std::endl; \
  } while (0)

void VectorInterface::SwitchModulus(IntType newModulus) {
  print_class("SwitchModulus[]");
  auto qDiff = newModulus - m_modulus;
  auto halfOldQ = m_modulus >> 1;
  for (size_t i = 0U; i < m_data.size(); ++i) {
    if (m_data[i] > halfOldQ) {
      m_data[i] += qDiff;
    }

    m_data[i] %= newModulus;
  }

  m_modulus = newModulus;
};

void VectorInterface::ModAddEq(std::vector<IntType>& otherVector) {
  print_class("ModAddEq[]");
  if (otherVector.size() != m_data.size()) {
    throw std::out_of_range("Vectors must be the same size");
  }

  // create new object from this object
  auto retVector(*this);

  for (size_t i = 0U; i < m_data.size(); ++i) {
    m_data[i] += otherVector[i];
    m_data[i] %= m_modulus;
  }

  return retVector.ModAddEq(otherVector);
}

CRTP& VectorInterface::ModAdd(std::vector<IntType>& otherVector) {
  print_class("ModAdd[]");
  CRTP retVector(*this);
  return retVector.ModAddEq(otherVector);
};

void VectorInterface::ModAddEQ(IntType scalar) {
  print_class("ModAddEq[] (scalar)");

  // create new object from this object
  auto retVector(*this);

  for (size_t i = 0U; i < m_data.size(); ++i) {
    m_data[i] += scalar;
    m_data[i] %= m_modulus;
  }
}

CRTP& VectorInterface::ModAdd(IntType scalar) {
  print_class("ModAdd[]");
  CRTP retVector(*this);
  return retVector.ModAddEq(scalar);
};

std::ostream& VectorInterface::operator<<(std::ostream& os) {
  this->Print(os);
  return os;
};

CRTP& VectorInterface::operator+(const CRTP& rhs) { return this->ModAdd(rhs); }

CRTP& VectorInterface::operator+=(const CRTP& rhs) { this->ModAddEq(rhs); }

friend CRTP& VectorInterface::operator+(const CRTP& lhs,
                                        const IntType& scalar) {
  return lhs.ModAdd(scalar);
}

friend CRTP& operator+(const IntType& scalar, const CRTP& rhs) {
  return rhs.ModAdd(scalar);
}

friend CRTP& operator+=(CRTP& lhs, const IntType& scalar) {
  lhs.ModAddEq(scalar);
}

IntType& operator[](size_t idx) {
  print_class("operator[]");
  return m_data[idx];
}

const IntType& operator[](size_t idx) const {
  print_class("operator[]");
  return m_data[idx];
}