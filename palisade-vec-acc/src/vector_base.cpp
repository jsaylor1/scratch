#include "vector_base.h"

using namespace vec_acc;


const std::string Base::m_className = "Base";

void Base::PureVirtual() {
    std::cout << "Base::PureVirtual" << std::endl;
    std::cout << "m_className: " << m_className << std::endl;
}