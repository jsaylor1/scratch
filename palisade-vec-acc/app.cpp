#include <iostream>
#include <memory>

#include "vector_extension.h"

int main(int argc, char const *argv[])
{
    std::cout << "vec_acc Example" << std::endl << std::endl;

    vec_acc::VectorBase<int64_t> base;
    std::cout << "Calling base.Abstract()" << std::endl;
    base.Abstract();
    std::cout << "Calling base.PureVirtual()" << std::endl;
    base.PureVirtual();
    std::cout << "Calling base.Override()" << std::endl;
    base.Override();

    std::cout << std::endl;

    vec_acc::VectorExtension<int64_t> ext;
    std::cout << "Calling ext.Abstract()" << std::endl;
    ext.Abstract();
    std::cout << "Calling ext.PureVirtual()" << std::endl;
    ext.PureVirtual();
    std::cout << "Calling ext.Override()" << std::endl;
    ext.Override();
    std::cout << "Calling ext.Override()" << std::endl;
    ext.Override();

    std::cout << std::endl;

    std::unique_ptr<vec_acc::Interface> interfacePointer(new vec_acc::Extension());
    std::cout << "Calling interfacePointer.Abstract()" << std::endl;
    interfacePointer->Abstract();
    std::cout << "Calling interfacePointer.PureVirtual()" << std::endl;
    interfacePointer->PureVirtual();
    std::cout << "Calling interfacePointer.Override()" << std::endl;
    interfacePointer->Override();

    std::cout << std::endl;

    std::unique_ptr<vec_acc::Base> basePointer(new vec_acc::Extension());
    std::cout << "Calling basePointer.Abstract()" << std::endl;
    basePointer->Abstract();
    std::cout << "Calling basePointer.PureVirtual()" << std::endl;
    basePointer->PureVirtual();
    std::cout << "Calling basePointer.Override()" << std::endl;
    basePointer->Override();

    
    return 0;
}
