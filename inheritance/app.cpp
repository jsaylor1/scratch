#include <iostream>
#include <memory>

#include "extension.h"

int main(int argc, char const *argv[])
{
    std::cout << "Inheritance Example" << std::endl << std::endl;

    inheritance::Base base;
    std::cout << "Calling base.Abstract()" << std::endl;
    base.Abstract();
    std::cout << "Calling base.PureVirtual()" << std::endl;
    base.PureVirtual();
    std::cout << "Calling base.Override()" << std::endl;
    base.Override();

    std::cout << std::endl;

    inheritance::Extension ext;
    std::cout << "Calling ext.Abstract()" << std::endl;
    ext.Abstract();
    std::cout << "Calling ext.PureVirtual()" << std::endl;
    ext.PureVirtual();
    std::cout << "Calling ext.Override()" << std::endl;
    ext.Override();

    std::cout << std::endl;

    std::unique_ptr<inheritance::Interface> interfacePointer(new inheritance::Extension());
    std::cout << "Calling interfacePointer.Abstract()" << std::endl;
    interfacePointer->Abstract();
    std::cout << "Calling interfacePointer.PureVirtual()" << std::endl;
    interfacePointer->PureVirtual();
    std::cout << "Calling interfacePointer.Override()" << std::endl;
    interfacePointer->Override();

    std::cout << std::endl;

    std::unique_ptr<inheritance::Base> basePointer(new inheritance::Extension());
    std::cout << "Calling basePointer.Abstract()" << std::endl;
    basePointer->Abstract();
    std::cout << "Calling basePointer.PureVirtual()" << std::endl;
    basePointer->PureVirtual();
    std::cout << "Calling basePointer.Override()" << std::endl;
    basePointer->Override();

    
    return 0;
}
