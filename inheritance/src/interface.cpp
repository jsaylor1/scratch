#include "interface.h"

using namespace inheritance;


const std::string Interface::m_className = "Interface";

void Interface::Abstract() {
    std::cout << "Interface::Abstract" << std::endl;
    std::cout << "m_className: " << m_className << std::endl;
}

void Interface::Override() {
    std::cout << "Interface::Override" << std::endl;
    std::cout << "m_className: " << m_className << std::endl;
}

void Interface::AddString( std::string newString ) {
    std::string addString(m_className + " adding: " + newString);
    m_stringVector.push_back(addString);
}

std::string Interface::PopString() {
    std::string ret = m_stringVector[m_stringVector.size() - 1];
    m_stringVector.pop_back();
    return ret;
}