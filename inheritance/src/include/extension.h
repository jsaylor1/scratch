#ifndef SRC_EXTENSION_H
#define SRC_EXTENSION_H

#include "base.h"

namespace inheritance {

    class Extension : public Base {
        static const std::string m_className;
      public:
        void PureVirtual();
        void Override();
    };

}

#endif // SRC_EXTENSION_H
