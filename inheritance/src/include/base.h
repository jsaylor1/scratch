#ifndef SRC_BASE_H
#define SRC_BASE_H

#include "interface.h"

namespace inheritance {    
    class Base : public Interface {
        static const std::string m_className;
      public:
        void PureVirtual();
    };
}

#endif // SRC_BASE_H
