#ifndef SRC_INTERFACE_H
#define SRC_INTERFACE_H

#include <iostream>
#include <string>
#include <vector>

namespace inheritance {

    class Interface {
        private:
            static const std::string m_className;
            std::vector<std::string> m_stringVector;
        public:
            virtual void PureVirtual() = 0;
            virtual void Abstract();
            virtual void Override();
            virtual void AddString( std::string newString );
            virtual std::string PopString();
    };
}

#endif // SRC_INTERFACE_H
