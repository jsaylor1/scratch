#include <iostream>
#include <memory>
#include <vector>
#include <type_traits>

#ifndef NDEBUG
	#define DEBUG(msg) \
		do { std::cerr << "[DEBUG] -- " << __FILE__ << ":" << __LINE__ << std::endl \
					   << "\t" << msg << std::endl; } while(0)
#else
	#define DEBUG(msg) (void) 0
#endif

#define FUNC_STAMP() \
	do {  std::cout << __FUNCTION__  << " -- " << __FILE__ << ":" << __LINE__  << std::endl; } while(0)

 /**
  * @brief Ideal Lattice (IL) Element (simplified)
  *
  * @tparam CRTP -- Curiously Reoccuring Template Pattern (CRTP) for returning derived type
  * @tparam intType -- Base type integer type
  */
template<typename CRTP, typename intType>
class ILElement {
public:

	using ILElementType = ILElement<CRTP, intType>;

	intType m_modulus; // modulus for the data, such that no value in data is greater than the modulus
	size_t m_ringDimension; // length of the data vector
	std::vector<intType> m_data; // container for the data

	ILElement() = delete;

	ILElement(intType modulus, size_t n_elements, intType value = 0) :
		m_modulus(modulus), m_data(n_elements, value) {
		FUNC_STAMP();
	}

	ILElement(const ILElement& copy) {
		FUNC_STAMP();
		DEBUG(copy);
		this->m_modulus = copy.m_modulus;
		this->m_data = copy.m_data;
	}

	// Passthrough size() from STL vector
	size_t size() { 
		FUNC_STAMP();
		return m_data.size(); 
	}

	intType operator[](size_t index) { 
		FUNC_STAMP();
		return m_data[index]; 
	}

	// In-place
	virtual void ModAddEq(const intType value) = 0;

	// Out-of-place
	virtual CRTP ModAdd(const intType value) {
		FUNC_STAMP();
		CRTP copy(*this);
		copy.ModAddEq(value);

		return copy;
	}

	virtual void print(std::ostream& os) const {
		FUNC_STAMP();
	}
};

template<typename CRTP, typename intType>
std::ostream& operator<< (std::ostream& os, const ILElement<CRTP, intType>& p)
{
	// FUNC_STAMP();
	p.print(os);
	return os;
}

/**
 * @brief DCRTPoly - simplified
 * 
 * NOTE - this is not replicating the RNS - i.e. it doesn't have a vector of vectors
 * 
 * @tparam intType 
 */
template<typename intType>
class DCRTPolyImpl : public ILElement<DCRTPolyImpl<intType>, intType>
{
public:

    using IntType = intType;
	using ILElementType = ILElement<DCRTPolyImpl<intType>, intType>;
	using DCRTPolyType = DCRTPolyImpl<intType>;

	DCRTPolyImpl() = delete;

	DCRTPolyImpl(intType modulus, size_t n_elements, intType value = 0) :
		ILElementType(modulus, n_elements, value) {
		FUNC_STAMP();
	}

	DCRTPolyImpl(const ILElementType& copy) : ILElementType(copy) {
		FUNC_STAMP();
		DEBUG("copy : " << copy);
	}

	DCRTPolyImpl(const DCRTPolyImpl& copy) : ILElementType(copy) {
		FUNC_STAMP();
		DEBUG("copy : " << copy);
	}

	virtual void ModAddEq(const intType value) override {
		FUNC_STAMP();

		for (size_t i = 0; i < this->m_data.size(); ++i) {
			this->m_data[i] = (this->m_data[i] + value) % this->m_modulus;
		}

		DEBUG(*this);
	}

	virtual void print(std::ostream& os) const {
		FUNC_STAMP();
		os << "{\"modulus\" : " << this->m_modulus << ",";
		os << "\"data\" : [";
		size_t len = this->m_data.size() - 1;
		for (size_t i = 0; i < len; ++i) {
			os << this->m_data[i] << ", ";
		}
		os << this->m_data[len] << "]}";
	}
};


// template<typename intType> class DCRTPolyImpl : public DCRTPolyImpl<intType> {};
/**
 * @brief Example of how we can inherit DCRTPolyImpl
 * 
 * @tparam intType 
 */
template<typename intType>
class HexlDCRTPolyImpl : public DCRTPolyImpl<intType> {
public:
    
    // This is the type that the CRTP template is expecting (to satisfy compiler)
	using DCRTPolyType = DCRTPolyImpl<intType>;
    
    // This is the base level, however it doesn't seem like we can access it directly
    // from this class
	using ILElementType = ILElement<DCRTPolyType, intType>;

	HexlDCRTPolyImpl(intType modulus, size_t n_elements, intType value = 0)
		: DCRTPolyType(modulus, n_elements, value) { 
			FUNC_STAMP();
		}

	/**
	 * @brief Construct a new Hexl D C R T Poly Impl object
	 * 
	 * This constructor is required to do the base pointer assignment
	 * e.g.
	 * 
	 * 	std::shared_ptr<HexlDCRTPolyImpl<T>> = std::make_shared<ILElement<T1,T2>>(ptr->ModAdd(value))
	 * 
	 * @param dcrtPoly 
	 */
	HexlDCRTPolyImpl(DCRTPolyType dcrtPoly)
		: DCRTPolyType(dcrtPoly) { 
			FUNC_STAMP();
		}

    /**
     * @brief This operator allows a the following
     * 
     * (HexlDCRTPolyImpl) hexl = (DCRTPolyImpl) base
     * 
     * Which is needed for this implementation style because
     * HexlDCRTPolyImpl::ModAdd returns a DCRTPolyImpl object
     * 
     * @param superT 
     * @return HexlDCRTPolyImpl& 
     */
	HexlDCRTPolyImpl& operator=(const DCRTPolyType& superT) {
		FUNC_STAMP();

		this->m_modulus = superT.m_modulus;
		this->m_data = superT.m_data;

		return *this;
	}

    /**
     * @brief HexlDCRTPolyImpl::ModAdd must return type DCRTPolyImpl to satisfy the compile
     * 
     * @param value 
     * @return DCRTPolyType 
     */
	virtual void ModAddEq(const intType value) override {
		FUNC_STAMP();

		intType v(value);

		if (v > this->m_modulus) {
			v %= this->m_modulus;
		}

		for (size_t i = 0; i < this->m_data.size(); ++i) {
			this->m_data[i] += v;
			if (this->m_data[i] > this->m_modulus) {
				this->m_data[i] -= this->m_modulus;
			}
		}

		DEBUG(*this);
	}

	/**
	 * @brief Have to override the base class (ilelement) implementation because CRTP will always be DCRTPoly
	 * 			and therefore the HexlDCRTPoly::ModAddEq will never be called currently. 
	 * 			(See ILElemnt<T1, T2>::ModAdd implementation for why this can work with inherited type)
	 * 
	 * @param value 
	 * @return DCRTPolyType 
	 */
	virtual DCRTPolyType ModAdd(const intType value) override {
		FUNC_STAMP();
		HexlDCRTPolyImpl copy(*this);
		copy.ModAddEq(value);

		return copy;
	}
};

/**
 * Mimic the PALISADE aliases
 */
using NativeInt = uint64_t;
#if 1
using DCRTPoly = DCRTPolyImpl<NativeInt>;
#else
using DCRTPoly = HexlDCRTPolyImpl<NativeInt>;
#endif

int main(int argc, char const* argv[])
{
	std::cout << "Curiously Reocurring Template Pattern (CRTP) Example" << std::endl;
	std::cout << "\t(More generally referred to as F-bound polymorphism)" << std::endl;

	// modulus hardcoded
	NativeInt q(16);
	// ring dimension (vector length)
	size_t n(1 << 5);

	using ilelement = ILElement<DCRTPoly, DCRTPoly::IntType>;

	/**
	 * Test simplified DCRTPolyImpl class
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test DCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	DCRTPolyImpl<NativeInt> dcrtPolyImpl(q, n);
	std::cout << dcrtPolyImpl << std::endl;
	dcrtPolyImpl = dcrtPolyImpl.ModAdd(23U);
	std::cout << dcrtPolyImpl << std::endl << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;


	/**
	 * Test ambigious pointer (Currently Not Working... getting 0's after ModAdd, expecting 7's)
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << "Test ambigious pointer to DCRTPoly" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	std::unique_ptr<ILElement<DCRTPolyImpl<NativeInt>, NativeInt>> ptr(new DCRTPolyImpl<NativeInt>(q, n));
	std::cout << *ptr << std::endl;
	ptr->ModAddEq(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << *ptr << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;


	/**
	 * Test Derived class of HexlDCRTPolyImpl 
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test HexlDCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	HexlDCRTPolyImpl<NativeInt> hexlPoly(q, n);
	std::cout << hexlPoly << std::endl;
	hexlPoly = hexlPoly.ModAdd(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << hexlPoly << std::endl << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;


	/**
	 * Test ambigious pointer (Currently Not Working... getting 0's after ModAdd, expecting 7's)
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test ambigious pointer to HexlDCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	ptr.reset(new HexlDCRTPolyImpl<NativeInt>(q, n));
	std::cout << *ptr << std::endl;
	ptr->ModAddEq(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << *ptr << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;


	/**
	 * Demo DCRTPoly alias
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << "Test DCRTPoly Alias" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	DCRTPoly dcrtPoly(q, n);
	std::cout << dcrtPoly << std::endl;
	dcrtPoly = dcrtPoly.ModAdd(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << dcrtPoly << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;


	/**
	 * Test ambigious pointer (Currently Not Working... getting 0's after ModAdd, expecting 7's)
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test ambigious pointer to DCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	ptr.reset(new HexlDCRTPolyImpl<NativeInt>(q, n));
	std::cout << *ptr << std::endl;
	ptr = std::make_unique<DCRTPoly>(ptr->ModAdd(23U));
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << *ptr << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;


	/**
	 * Test ambigious pointer (Currently Not Working... getting 0's after ModAdd, expecting 7's)
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test ambigious pointer to HexlDCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	ptr.reset(new HexlDCRTPolyImpl<NativeInt>(q, n));
	std::cout << *ptr << std::endl;
	ptr = std::make_unique<HexlDCRTPolyImpl<NativeInt>>(ptr->ModAdd(23U));
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << *ptr << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;
	
	return 0;
}
