#include <iostream>
#include <memory>
#include <vector>
#include <type_traits>

// Dummy struct to end recursive CRTP
template <typename intType>
struct Crtp_Final_Impl;

/**
 * @brief
 *
 * @tparam THIS - Current object being defined
 * @tparam IMPL - The template argument
 * @tparam SUPERCLASS - The object being inherited
 */
 // template <template <typename> class THIS, typename IMPL,
 //           template <typename> class SUPERCLASS, typename intType>
 // using Crtp_Find_Impl = SUPERCLASS<
 //     std::conditional_t<
 //         std::is_same_v<
 //             IMPL<intType>, 
 //             Crtp_Final_Impl<intType>,
 //             THIS<Crtp_Final_Impl<intType>, 
 //             IMPL>>;

 /**
  * @brief Ideal Lattice (IL) Element
  *
  * @tparam intType -- Base type integer type
  * @tparam T -- Curiously Reoccuring Template Pattern (CRTP) for returning derived type
  */
template<typename CRTP, typename intType>
class ILElement {
public:
    using CRTP_ptr = std::shared_ptr<CRTP>;

	intType m_modulus;
	std::vector<intType> m_data;

	ILElement() = delete;

	ILElement(intType modulus, size_t n_elements, intType value = 0) :
		m_modulus(modulus), m_data(n_elements, value) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}

	ILElement(const ILElement& copy) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		this->m_modulus = copy.m_modulus;
		this->m_data = copy.m_data;
	}

	// Passthrough size() from STL vector
	size_t size() { return m_data.size(); }

	intType operator[](size_t index) { return m_data[index]; }

	virtual std::shared_ptr<CRTP> ModAdd(const intType value) = 0;

	virtual void print(std::ostream& os) const {
		os << "ILElement" << std::endl;
	}
};

template<typename CRTP, typename intType>
std::ostream& operator<< (std::ostream& os, const ILElement<CRTP, intType>& p)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	p.print(os);
	return os;
}

// template<typename intType, typename CRTP = Crtp_Final_Impl<intType>>
// class DCRTPolyImpl : Crtp_Find_Impl<DCRTPolyImpl, CRTP, ILElement>
template<typename intType>
class DCRTPolyImpl : public ILElement<DCRTPolyImpl<intType>, intType>
{
public:

    using IntType = intType;
	using ILElementType = ILElement<DCRTPolyImpl<intType>, intType>;
	using DCRTPolyType = DCRTPolyImpl<intType>;
    using DCRTPolyPtr = std::shared_ptr<DCRTPolyType>;
    #define MakeDCRTPolyPtr(obj) std::make_shared<DCRTPolyType>(obj);

	DCRTPolyImpl(intType modulus, size_t n_elements, intType value = 0) :
		ILElementType(modulus, n_elements, value) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}

	DCRTPolyImpl(const DCRTPolyImpl& dcrtPoly) : ILElementType(dcrtPoly) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}

	virtual DCRTPolyPtr ModAdd(const intType value) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		auto ret = MakeDCRTPolyPtr(*this);

		for (size_t i = 0; i < ret->m_data.size(); ++i) {
			ret->m_data[i] = (ret->m_data[i] + value) % ret->m_modulus;
		}

		return ret;
	}

	virtual void print(std::ostream& os) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		os << "\"data\" : [";
		size_t len = this->m_data.size() - 1;
		for (size_t i = 0; i < len; ++i) {
			os << this->m_data[i] << ", ";
		}
		os << this->m_data[len] << "]";
	}
};


// template<typename intType> class DCRTPolyImpl : public DCRTPolyImpl<intType> {};
/**
 * @brief Example of how we can inherit DCRTPolyImpl
 * 
 * @tparam intType 
 */
template<typename intType>
class HexlDCRTPolyImpl : public DCRTPolyImpl<intType> {
public:

    using DCRTPolyType     = DCRTPolyImpl<intType>;
    using HexlDCRTPolyType = HexlDCRTPolyImpl<intType>;

	HexlDCRTPolyImpl(intType modulus, size_t n_elements, intType value = 0)
		: DCRTPolyType(modulus, n_elements, value) { }

    /**
     * @brief This operator allows a the following
     * 
     * (HexlDCRTPolyImpl) hexl = (DCRTPolyImpl) base
     * 
     * Which is needed for this implementation style because
     * HexlDCRTPolyImpl::ModAdd returns a DCRTPolyImpl object
     * 
     * @param superT 
     * @return HexlDCRTPolyImpl& 
     */
	HexlDCRTPolyImpl& operator=(const DCRTPolyType& superT) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;

		this->m_modulus = superT.m_modulus;
		this->m_data = superT.m_data;

		return *this;
	}

    /**
     * @brief HexlDCRTPolyImpl::ModAdd hide the pure-virtual
     * 
     * @param value 
     * @return HexlDCRTPolyType 
     */
	HexlDCRTPolyType ModAdd(const intType value) {

		HexlDCRTPolyType ret(*this);
		intType v(value);

		if (v > ret.m_modulus) {
			v %= ret.m_modulus;
		}

		for (size_t i = 0; i < this->m_data.size(); ++i) {
			ret.m_data[i] += v;
			if (ret.m_data[i] > ret.m_modulus) {
				ret.m_data[i] -= ret.m_modulus;
			}
		}

		return ret;
    }
};

using NativeInt = uint64_t;
#if 0
using DCRTPoly = DCRTPolyImpl<NativeInt>;
#else
using DCRTPoly = HexlDCRTPolyImpl<NativeInt>;
#endif

int main(int argc, char const* argv[])
{
	std::cout << "Curiously Reocurring Template Pattern (CRTP) Example" << std::endl;
	std::cout << "\t(More generally referred to as F-bound polymorphism)" << std::endl;

	NativeInt q(16);
	size_t n(1 << 10);

	/**
	 * Test simplified DCRTPolyImpl class
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test ambigious pointer to DCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	DCRTPolyImpl<NativeInt> dcrtPolyImpl(q, n);
	std::cout << dcrtPolyImpl << std::endl;
	dcrtPolyImpl = *(dcrtPolyImpl.ModAdd(23U));
	std::cout << dcrtPolyImpl << std::endl << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;

	/**
	 * Test Derived class of DCRTPolyImpl pointer
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test ambigious pointer to HexlDCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	HexlDCRTPolyImpl<NativeInt> hexlPoly(q, n);
	std::cout << hexlPoly << std::endl;
	hexlPoly = hexlPoly.ModAdd(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << hexlPoly << std::endl << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;

	/**
	 * Test ambigious pointer (Currently Not Working... getting 0's after ModAdd, expecting 7's)
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << ">> Test ambigious pointer to HexlDCRTPolyImpl" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	ILElement<DCRTPolyImpl<NativeInt>, NativeInt>* ptr = new HexlDCRTPolyImpl<NativeInt>(q, n);
	std::cout << *ptr << std::endl;
	ptr->ModAdd(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << *ptr << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;
	delete ptr;

	std::cout << "=================================================================" << std::endl;
	std::cout << "Test ambigious pointer to DCRTPoly" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	ptr = new DCRTPoly(q, n);
	ptr->ModAdd(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << *ptr << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;

	// std::cout << "ptr out: ";
	// for(size_t i=0; i<ptr->size(); i++) {
	//     std::cout << ptr->operator[](i) << ",";
	// } 


	/**
	 * Demo DCRTPoly alias
	 */
	std::cout << "=================================================================" << std::endl;
	std::cout << "Test DCRTPoly Alias" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
	DCRTPoly dcrtPoly(q, n);
	std::cout << dcrtPoly << std::endl;
	dcrtPoly = dcrtPoly.ModAdd(23U);
	std::cout << ">>>>>>> Values should be all 7's <<<<<<<<" << std::endl;
	std::cout << dcrtPoly << std::endl;
	std::cout << "=================================================================" << std::endl << std::endl;

	return 0;
}
